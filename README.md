A basic application that displays different categories in ReactShop written in React Redux. 
The project utilizes Bootstrap and Material UI framework. Components used include Toolbar, Card, IconButton and Icons.

This application involved nested components structure. A Categories component connecting to CategoriesSerivce to retrieve list of categories from a json file and passing it to the CategoriesList component as a props. CategoriesList component then render a CategoryCard component with a selectCategory function.

<p>
The application is composed of the following components:
<ul>
<li>NavBar - to display application title on the left as well as a shopping-icon and account-icon on the right. </li>
<li>Content - to render CategoriesComponent.</li>
<li>Categories - to display page title and responsible for connecting CategoriesService to retrieve list of categories 
from json file by calling 'FETCH_CATEGORIES' action. Also selected category by user will be displayed below.</li>
<li>CategoriesList - responsible for rendering a list of category objects.</li>
<li>CategoryCard - responsible for rendering each category detail with a background image and category name overlay on it. An onClick function is involved by calling the 'SELECT_CATEGORY' action in the redux store.  
</ul>
</p>
<br/><br/>
![reactshop](/uploads/1cad111e6b643fd04c2a65fcc89cadc3/reactshop.JPG)


