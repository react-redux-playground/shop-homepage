import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';

import './styles/app.css';
import './styles/base.css';

import reducers from './reduxStore/reducers/AppReducers';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

const store = createStore(reducers, applyMiddleware(thunk));

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>, 
    document.getElementById('root')
);