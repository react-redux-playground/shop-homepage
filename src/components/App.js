import React from 'react';
import NavBar from './NavBar';
import Content from './Content';

const App = () => {
    return (
       <div className='app'>
            <NavBar></NavBar>
            <Content></Content>
       </div>
    );
}   

export default App;
