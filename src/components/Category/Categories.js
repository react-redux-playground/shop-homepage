import React from 'react';
import { connect } from 'react-redux';
import { fetchCategories } from '../../reduxStore/actions/CategoriesActions';
import { CategoriesList } from './CategoriesList';

export class Categories extends React.Component {
    componentDidMount() {
        this.props.fetchCategories();
    }

    render() {
        return (
            <div>
                <div className="page-title">Shop Categories</div>   

                <CategoriesList list={this.props.categories}></CategoriesList>
                
                <div style={{textAlign: 'center'}}>
                    {this.props.selectedCategory 
                        ? `You have selected ${this.props.selectedCategory.name}` 
                        : 'You can select a category'
                    }
                </div>
            </div>
        )
    }
}   

const mapStateToProps = state => {
    console.log('state ', state);
    return {
        categories: state.category.categories,
        selectedCategory: state.category.selectedCategory
    };
}

export default connect(mapStateToProps, { fetchCategories }) (Categories);