import React from 'react';
import CategoryCard from './CategoryCard';

export class CategoriesList extends React.Component {

    render() {
        return (
            <div className="row">
                {this.renderCategories()}
            </div>
        )
    }

    renderCategories() {
        return this.props.list.map(category => {
            return (
                <div className="col-4 mb-5" key={category.id}>
                    <CategoryCard  item={category}></CategoryCard>
                </div> 
            )
        });
    }
}   

export default CategoriesList;



// import React from 'react';
// import { connect } from 'react-redux';
// import { 
//     fetchCategories,
//     selectCategory
//  } from '../actions';

// import Card from '@material-ui/core/Card';
// import CardMedia from '@material-ui/core/CardMedia';
// import CardActionArea  from '@material-ui/core/CardActionArea';

// export class CategoriesList extends React.Component {

//     componentDidMount() {
//         this.props.fetchCategories();
//     }

//     render() {
//         return (
//             <div>
//                 <div className="page-title">Shop Categories</div>
//                 <div className="row">
//                     {this.renderCategories()}
//                 </div>
//                 <div style={{textAlign: 'center'}}>
//                     {this.props.selectedCategory 
//                         ? `You have selected ${this.props.selectedCategory.name}` 
//                         : 'You can select a category'
//                     }
//                 </div>
//             </div>
//         )
//     }

//     renderCategories() {
//         return this.props.categories.map(category => {
//             return (
//                 <div className="col-4 mb-5" key={category.id}>
//                     <Card>
//                         <CardActionArea onClick={() => this.props.selectCategory(category)}>
//                             <CardMedia component="img"
//                                         height="140" width="200"
//                                         src={category.imgPath}   />
//                             <div className="img-overlay">
//                                 {category.name}
//                             </div>
//                         </CardActionArea>  
//                     </Card>
//                 </div>
//             )
//         });
//     }
// }   

// const mapStateToProps = state => {
//     console.log('state ', state);
//     return {
//         categories: state.category.categories,
//         selectedCategory: state.category.selectedCategory
//     };
// }

// export default connect(mapStateToProps, { 
//     fetchCategories, 
//     selectCategory 
// }) (CategoriesList);