import React from 'react';
import { connect } from 'react-redux';

import { selectCategory } from '../../reduxStore/actions/CategoriesActions';

import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardActionArea  from '@material-ui/core/CardActionArea';

export class CategoryCard extends React.Component {
    render() {
        const {item} = this.props;
      
        return (
            <div>
                <Card>
                    <CardActionArea onClick={() => this.props.selectCategory(item)}>
                        <CardMedia component="img"
                                    height="140" width="200"
                                    src={item.imgPath}   />
                        <div className="img-overlay">
                            {item.name}
                        </div>
                    </CardActionArea>  
                </Card>
            </div>
        )
    }
}

export default connect (null, {selectCategory})(CategoryCard);