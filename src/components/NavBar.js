import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';
import ShoppingCartOutlined from '@material-ui/icons/ShoppingCartOutlined';

const NavBar = () => {
    return (
       <div className="sticky-top">
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6">
                        <div className='navbar-logo fxRow fxAlignCenter'>
                            <span className="logo-text">ReactShop</span>
                        </div>
                    </Typography>

                    <div className='spacer'></div>

                    <IconButton color="inherit">
                        <ShoppingCartOutlined/>
                    </IconButton>

                    <IconButton color="inherit">
                        <AccountCircle />
                    </IconButton>
                </Toolbar>
            </AppBar>
        </div>  
    )
}

export default NavBar;