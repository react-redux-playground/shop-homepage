import React from 'react';
import Categories from './Category/Categories';

const Content = () => {
    return (
        <div className='content'>
            <Categories></Categories>
        </div>
    )
}

export default Content;