import categories from './categories.json'

export default class CategoriesService {
    static getCategories() {
        return categories ? categories : [];
    }
}