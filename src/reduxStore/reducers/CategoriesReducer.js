const initialState = {
    categories: [],
    selectedCategory: null
}

export const CategoriesReducer = (state = initialState, action) => {
    switch (action.type) {    
        case 'FETCH_CATEGORIES':
            console.log('call fetch categories reducer');
            return {
                ...state,
                categories: action.payload,
                selectedCategory: null
            };
        
        case 'SELECT_CATEGORY':
            console.log('call select category reducer');
            return {
                ...state,
                categories: state.categories,
                selectedCategory: action.payload
            };

        default:    
            return state;   
    }
} 

