import CategoriesService from '../../services/CategoriesService';

export const fetchCategories = () => dispatch => {
    const response =  CategoriesService.getCategories();

    dispatch({
        type: 'FETCH_CATEGORIES',
        payload: response
    });
};
   

export const selectCategory = category => {
    return {
        type: 'SELECT_CATEGORY',
        payload: category
    };
}